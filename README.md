#Halo!

#Masukkan Data
train_o <- read.csv("D:/Works/Hackathon BPS/bigdata-bps-hackathon-2021/train-online_booking_2020.csv")
View(train_o)

sum(is.na(train_o))

#Menggabungkan Rated dan Unrated
library(data.table)
train_oc<-setDT(train_o)[,.(all_avalailable_room = sum(all_available_room), room_total = sum(room_total)), by= .(tanggal)]

#Menghitung jumlah data
library(dplyr)
count(train_oc)

#Melengkapi periode waktu
library(tidyr)
train_oc <- train_oc %>% 
  mutate(Date = as.Date(tanggal, format = "%m/%d/%Y"))
train_oc <- train_oc %>%
  complete(Date = seq.Date(min(Date), max(Date), by="day"))

#Imputasi
library(imputeTS)
train_oc[train_oc==0]<-NA
train_oc$all_avalailable_room<-na_interpolation(train_oc$all_avalailable_room)
train_oc$room_total<-na_interpolation(train_oc$room_total)

#Bookedrate
train_oc$book<-1-(train_oc$all_avalailable_room/train_oc$room_total)

#Variabel Hari
train_oc$hari<-rep(c(3,4,5,6,7,1,2),length.out=366)
train_oc$minggu<-rep(c(0,0,0,0,1,0,0),length.out=366)
train_oc$sabtu<-rep(c(0,0,0,1,0,0,0),length.out=366)
train_oc$jumat<-rep(c(0,0,1,0,0,0,0),length.out=366)

#Variabel Pandemi

train_oc<-train_oc %>%
  mutate(covid=1)
train_oc$covid[1:61]<-0

train_oc<-train_oc %>%
  mutate(covidbali=1)
train_oc$covidbali[1:70]<-0

#Variabel Bulan
library(lubridate)
train_oc$Bulan<-month(train_oc$Date)

#Menggabungkan Bulan
train_ocb<-setDT(train_oc)[,.(book = mean(book), hari = mean(hari), minggu = mean(minggu), sabtu = mean(sabtu), jumat=mean(jumat), covid=mean(covid), covidbali=mean(covidbali)), by= .(Bulan)]
train_ocb$series<-1:12

plot(train_ocb)

#Memasukkan Data Ex Google Trend
GoogleBali <- read.csv("D:/Works/Hackathon BPS/GoogleBali.csv", sep=";")
train_ocb$GoogleBali<-GoogleBali$Bali...Seluruh.dunia.[193:204]
GoogleHotel <- read.csv("D:/Works/Hackathon BPS/GoogleHotelinBali.csv", sep=";")
train_ocb$GoogleHotel<-GoogleHotel$Hotel...Bali.[193:204]

#Memasukkan data TPK
train.TPK_Hotel_berbintang_2020 <- read.csv("D:/Works/Hackathon BPS/bigdata-bps-hackathon-2021/train-TPK_Hotel_berbintang_2020.csv")
train_ocb$tpk<-train.TPK_Hotel_berbintang_2020$TPK

#Pemodelan
model1<-lm(tpk~Bulan+book+covid+covidbali+series+jumat+sabtu+minggu, data=train_ocb)

#-----Prediksi
test_o <- read.csv("D:/Works/Hackathon BPS/bigdata-bps-hackathon-2021/test-online_booking_2021.csv")

#Menggabungkan Rated dan Unrated
test_oc<-setDT(test_o)[,.(all_avalailable_room = sum(all_available_room), room_total = sum(room_total)), by= .(tanggal)]

#Menghitung jumlah data
count(test_oc)

#Melengkapi periode waktu
test_oc <- test_oc %>% 
  mutate(Date = as.Date(tanggal, format = "%m/%d/%Y"))
test_oc <- test_oc %>%
  complete(Date = seq.Date(min(Date), max(Date), by="day"))
count(test_oc)

#Imputasi
test_oc[test_oc==0]<-NA
test_oc$all_avalailable_room<-na_interpolation(test_oc$all_avalailable_room)
test_oc$room_total<-na_interpolation(test_oc$room_total)

#Bookedrate
test_oc$book<-1-(test_oc$all_avalailable_room/test_oc$room_total)

#Variabel Hari
test_oc$hari<-rep(c(5,6,7,1,2,3,4),length.out=181)
test_oc$minggu<-rep(c(0,0,1,0,0,0,0),length.out=181)
test_oc$sabtu<-rep(c(0,1,0,0,0,0,0),length.out=181)
test_oc$jumat<-rep(c(1,0,0,0,0,0,0),length.out=181)

#Variabel Pandemi

test_oc<-test_oc %>%
  mutate(covid=0)

test_oc<-test_oc %>%
  mutate(covidbali=0)

#Variabel Bulan
library(lubridate)
test_oc$Bulan<-month(test_oc$Date)

#Menggabungkan Bulan
test_ocb<-setDT(test_oc)[,.(book = mean(book), hari = mean(hari), minggu = mean(minggu), sabtu = mean(sabtu), jumat=mean(jumat), covid=mean(covid), covidbali=mean(covidbali)), by= .(Bulan)]
test_ocb$series<-13:18

plot(test_ocb)


#Prediksi
library(forecast)
test_ocb <-test_ocb %>%
  mutate(tpk=NA)
hasil1<-predict(model1, newdata=test_ocb)
hasil1

#Submisi
sample_submission <- read_csv("D:/Works/Hackathon BPS/bigdata-bps-hackathon-2021/sample-submission.csv")
sample_submission$TPK<-hasil1

#Eksport ke Excel
library(writexl)
write.csv(sample_submission,"D:/Works/Hackathon BPS/submisi3.csv",row.names=F)
